import React, { Component } from "react";
import {
  View,
  Platform
} from 'react-native';
import { createStackNavigator,Header } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import Theme from "../src/style/Theme"
import MemberScene from "./scene/MemberScene";
import AddMemberScene from "./scene/AddMemberScene";

const Stack = createStackNavigator();
const screenOptionStyle = {
  headerBackTitleVisible: false,
  headerTintColor: Theme.color.color_white,
  headerStyle:{
    backgroundColor: Theme.color.color_primary
  },
  headerBackTitleVisible: true,
  headerBackTitle: 'Back'
};

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      is_loading: false,
    }
  }

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator screenOptions={screenOptionStyle}>
          <Stack.Screen 
            name="Member"
            component={MemberScene} options={{ headerShown: true, title: 'My Members'}}/>
          <Stack.Screen 
            name="AddMember" 
            component={AddMemberScene} options={{ headerShown: true, title: 'Add new members'}}/>
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

export default App;