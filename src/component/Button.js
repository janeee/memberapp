import React from "react";
import { Text, TouchableOpacity, View, Image } from "react-native";
import Theme from "../style/Theme";

export const ButtonFill = (props) => {
  return (
    <TouchableOpacity
      disabled={ props.disable ? props.disable : false}
      {...props}
      style={{
        height: 40,
        borderRadius: props.borderRadius ? props.borderRadius : 5,
        shadowColor: "#000",
        shadowOffset: { width: 1, height: 2 },
        shadowOpacity: 0.4,
        shadowRadius: 2,
        elevation: 1,
        backgroundColor: props.backgroundColor || Theme.color.color_primary,
        justifyContent: "center",
        alignItems: "center",
        ...props.style,
      }}
    >
      <View style={{ flexDirection: "row" }}>
        {props.icon && (
          <Image
            style={{ width: 18, height: 18, marginEnd: 8, alignSelf:'center', resizeMode: "contain", tintColor: props.iconTintColor ? props.iconTintColor : "white" }}
            source={props.icon}
          />
        )}
        <Text
          style={{
            fontSize: Theme.size.title_third,
            color: props.labelColor ? props.labelColor : "white",
          }}
        >
          {props.label}
        </Text>
      </View>
    </TouchableOpacity>
  );
};