import React from "react";
import { Text, TouchableOpacity, View, Image } from "react-native";
import Theme from "../style/Theme";

export const MemberItem = (props) => {
  return (
    <View
      style={{
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: { width: 1, height: 2 },
        shadowOpacity: 0.4,
        shadowRadius: 2,
        elevation: 1,
        backgroundColor: props.backgroundColor || Theme.color.color_primary,
        padding: 16,
        margin: 4,
        ...props.style,
      }}
    >
      <View style={{ flexDirection: 'row', justifyContent:'space-between' }}>
        <View>
          <Text
            style={{
              fontSize: Theme.size.title_main,
              color: Theme.color.color_white,
              fontWeight: 'bold'
            }}
          >
            {props.name}
          </Text>
          <Text
            style={{
              fontSize: Theme.size.title_second,
              color: Theme.color.color_white,
            }}
          >
            {props.id}
          </Text>
          <Text
            style={{
              fontSize: Theme.size.title_second,
              color: Theme.color.color_white,
            }}
          >
            {props.phone}
          </Text>
        </View>
        <View style={{justifyContent: 'space-between'}}>
          <TouchableOpacity onPress={props.onPressEdit}>
            <Image
              style={{ width: 24, height: 24, alignSelf:'center', resizeMode: "contain", tintColor: "white" }}
              source={require("../resource/img/pen.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={props.onPressDelete}>
            <Image
              style={{ width: 24, height: 24, alignSelf:'center', resizeMode: "contain", tintColor: "red" }}
              source={require("../resource/img/delete.png")}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};