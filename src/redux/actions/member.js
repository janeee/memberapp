import { MEMBER, ADD_MEMBER, DEL_MEMBER, EDIT_MEMBER } from "../../constance/Constance";

export function featchingMember() {
  return {
    type: MEMBER,
  };
}

export function addMember(memberData) {
  return {
    type: ADD_MEMBER,
    payload: memberData,
  };
}

export function deleteMember(index) {
  return {
    type: DEL_MEMBER,
    payload: index,
  };
}

export function editMember(index, dataMember) {
  return {
    type: EDIT_MEMBER,
    payload: { index: index, data: dataMember },
  };
}
