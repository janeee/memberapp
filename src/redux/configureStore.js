import { createStore, combineReducers } from "redux";
import memberReducer from "./reducers/memberReducer";

const rootReducer = combineReducers({
  featchmember: memberReducer,
});
const configureStore = () => {
  return createStore(rootReducer);
};
export default configureStore;
