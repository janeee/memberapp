import { MEMBER, ADD_MEMBER, DEL_MEMBER, EDIT_MEMBER } from "../../constance/Constance";

const initialState = {
  featchmember: [],
};
const memberReducer = (state = initialState, action) => {
  switch (action.type) {
    case MEMBER:
      return {
        ...state,
        featchmember: state.featchmember,
      };
    case ADD_MEMBER:
      state.featchmember.push(action.payload)
      return {
        ...state,
        featchmember: state.featchmember,
      };
    case DEL_MEMBER:
      state.featchmember.splice(action.payload, 1)
      return {
        ...state,
        featchmember: state.featchmember,
      };
    case EDIT_MEMBER:
      state.featchmember[action.payload.index] = action.payload.data
      return {
        ...state,
        featchmember: state.featchmember,
      };
    default:
      return state;
  }
};
export default memberReducer;
