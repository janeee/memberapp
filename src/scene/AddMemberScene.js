import React from "react";
import {
  Dimensions,
  SafeAreaView,
  Image,
  Platform,
  TextInput
} from "react-native";
import { StackActions } from "@react-navigation/native";
import BaseScreenComponent from "../component/BaseScreenComponent";
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
import { ButtonFill } from "../component/Button"
import Theme from "../style/Theme"
import {
  TextField,
  FilledTextField,
  OutlinedTextField,
} from '../react-native-material-textfield';
import { KeyboardAwareScrollView } from "@codler/react-native-keyboard-aware-scroll-view";
import { CommonActions } from '@react-navigation/native';

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as memberActions from "../redux/actions/member";

class AddMemberScene extends BaseScreenComponent {

  constructor(props) {
    super(props);
    this.state = {
      error: {
        phone: false,
        id: false,
        name: false
      }
    }
  }

  componentDidMount() {
    this.initNavigation()
    const item = this.props.route.params.item
    if (item) {
      this.fieldName.setValue(item.name)
      this.fieldID.setValue(item.id)
      this.fieldPhone.setValue(item.phone)
      this.setState({})
    }
  }

  componentDidUpdate() {
  }

  initNavigation() {
    this.props.navigation.setOptions({
      title: this.props.route.params.title,
    })
  }

  formatPhone = (text) => {
    var phoneRe = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    var digits = text.replace(/\D/g, "");
    if (phoneRe.test(digits)) {
      this.setState({
        error: {
          ...this.state.error,
          phone: false
        }
      })
    }
    else {
      this.setState({
        error: {
          ...this.state.error,
          phone: true
        }
      })
    }
  };

  formatPhoneNumber(input) {
    input = input.replace(/\D/g, '').substring(0, 10); //Strip everything but 1st 10 digits
    var size = input.length;
    if (size > 0) { input = input }
    if (size > 3) { input = input.slice(0, 3) + "-" + input.slice(3) }
    if (size > 6) { input = input.slice(0, 7) + "-" + input.slice(7) }
    return input;
  }

  formatID = (text) => {
    var idRe = /^[\+]?[(]?[0-9]{1}[)]?[-\s\.]?[0-9]{4}[-\s\.]?[-\s\.]?[0-9]{5}[-\s\.]?[-\s\.]?[0-9]{2}[-\s\.]?[0-9]{1}$/im;
    var digits = text.replace(/\D/g, "");
    if (idRe.test(digits)) {
      this.setState({
        error: {
          ...this.state.error,
          id: false
        }
      })
    }
    else {
      this.setState({
        error: {
          ...this.state.error,
          id: true
        }
      })
    }
  };

  formatIDNo(input) {
    input = input.replace(/\D/g, '').substring(0, 13); //Strip everything but 1st 10 digits
    var size = input.length;
    if (size > 0) {
      input = input
    }
    if (size > 1) {
      input = input.slice(0, 1) + "-" + input.slice(1)
    }
    if (size > 5) {
      input = input.slice(0, 6) + "-" + input.slice(6)
    }
    if (size > 10) {
      input = input.slice(0, 12) + "-" + input.slice(12)
    }
    if (size > 12) {
      input = input.slice(0, 15) + "-" + input.slice(15)
    }
    return input;
  }

  render() {

    return (
      <KeyboardAwareScrollView
        contentContainerStyle={{ flexGrow: 1, justifyContent: 'flex-start', paddingStart: 16, paddingEnd: 16 }}
        showsVerticalScrollIndicator={false}
        scrollEnabled={false}
      >
        <TextField
          label='NAME'
          keyboardType='default'
          fontSize={Theme.size.title_third}
          ref={ref => this.fieldName = ref}
        />
        <TextField
          label='ID'
          keyboardType='phone-pad'
          formatText={this.formatIDNo}
          onChangeText={this.formatID}
          error={this.state.error.id ? "Validate id card number" : ""}
          fontSize={Theme.size.title_third}
          ref={ref => this.fieldID = ref}
        />
        <TextField
          label='PHONE NUMBER'
          keyboardType='phone-pad'
          formatText={this.formatPhoneNumber}
          onChangeText={this.formatPhone}
          error={this.state.error.phone ? "Validate phone number" : ""}
          fontSize={Theme.size.title_third}
          ref={ref => this.fieldPhone = ref}
        />
        <ButtonFill label={"Submit"} disable={this.fieldName && this.fieldID && this.fieldPhone && !this.state.error.id && !this.state.error.phone ? false : true} style={{ marginStart: 64, marginEnd: 64, marginTop: 16 }} backgroundColor={this.fieldName && this.fieldID && this.fieldPhone && !this.state.error.id && !this.state.error.phone ? Theme.color.color_primary : 'grey'} onPress={() => {
          if (this.props.route.params.item) {
            this.props.actions.editMember(
              this.props.route.params.index,
              {
                name: this.fieldName.state.text,
                id: this.fieldID.state.text,
                phone: this.fieldPhone.state.text
              }
            )
          }
          else {
            this.props.actions.addMember(
              {
                name: this.fieldName.state.text,
                id: this.fieldID.state.text,
                phone: this.fieldPhone.state.text
              }
            )
          }
          this.props.actions.featchingMember()
          this.props.navigation.dispatch(CommonActions.goBack());
        }} />
      </KeyboardAwareScrollView>
    );
  }
}

const mapStateToProps = (state) => ({
  featchmember: state.featchmember,
});

const ActionCreators = Object.assign({}, memberActions);
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddMemberScene);