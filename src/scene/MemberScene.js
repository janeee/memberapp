import React from "react";
import {
  Dimensions,
  SafeAreaView,
  FlatList,
  Text,
  View,
  Alert
} from "react-native";
import { StackActions } from "@react-navigation/native";
import BaseScreenComponent from "../component/BaseScreenComponent";
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
import { CommonActions } from '@react-navigation/native';
import { ButtonFill } from "../component/Button"
import Theme from "../style/Theme"
import { MemberItem } from "../component/Items"

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as memberActions from "../redux/actions/member";

class MemberScene extends BaseScreenComponent {

  constructor(props) {
    super(props);
  }

  componentDidMount() {

  }

  componentDidUpdate() {
  }

  showAlert(name, index) {

    Alert.alert(
      "Confirmation",
      `Are you sure to delete ${name}`,
      [
        {
          text: "Cancel",
        },
        {
          text: "OK",
          onPress: () => {
            this.props.actions.deleteMember(index)
          }
        }
      ],
      { cancelable: false }
    );
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: Theme.color.color_white, justifyContent: this.props.featchmember.featchmember.length < 1 ? 'center' : 'flex-start' }}>
        {
          this.props.featchmember.featchmember.length < 1 ?
            <ButtonFill label={"Add your first members"} style={{ width: screenWidth - 56, alignSelf: 'center', }} backgroundColor={Theme.color.color_primary} onPress={() => {
              this.props.navigation.dispatch(
                CommonActions.navigate({
                  name: 'AddMember',
                  params: {
                    title: "Add new members",
                  }
                })
              );
            }} />
            :
            <View style={{ flex: 1 }}>
              <FlatList
                style={{ paddingStart: 24, paddingEnd: 24, marginTop: 8, marginBottom: 8 }}
                data={this.props.featchmember.featchmember}
                keyExtractor={(item, index) => {
                  return index.toString();
                }}
                renderItem={({ item, index }) => {
                  return <MemberItem name={item.name} id={item.id} phone={item.phone} onPressDelete={() => {
                    this.showAlert(item.name, index)
                  }} onPressEdit={() => {
                    this.props.navigation.dispatch(
                      CommonActions.navigate({
                        name: 'AddMember',
                        params: {
                          title: "Edit members",
                          item: item,
                          index: index
                        }
                      })
                    );
                  }} />
                }}
              ></FlatList>
              <ButtonFill label={"Add members"} style={{ width: screenWidth - 56, alignSelf: 'center', borderColor: Theme.color.color_primary, borderWidth: 1 }} labelColor={Theme.color.color_primary} icon={require("../resource/img/plus.png")} iconTintColor={Theme.color.color_primary} backgroundColor={Theme.color.color_white} onPress={() => {
                this.props.navigation.dispatch(
                  CommonActions.navigate({
                    name: 'AddMember',
                    params: {
                      title: "Add new members"
                    }
                  })
                );
              }} />
            </View>
        }
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => ({
  featchmember: state.featchmember,
});

const ActionCreators = Object.assign({}, memberActions);
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(MemberScene);
