const size = {
    title_main: 18,
    title_second: 16,
    title_third: 13,
    detail_main: 11,
    detail_second: 10,
    h1: 24
};

const color = {
    color_primary: "#007aff",
    color_black: "#000000",
    color_white: "#ffffff",
};

const shadow = {
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
}

const cardPrimary = {
    ...shadow,
    backgroundColor: color.color_primary,
    borderRadius:15,
    elevation: 5
}

const cardWhite = {
    ...shadow,
    backgroundColor: color.color_white,
    borderRadius:15,
    elevation: 5
}

const fontFamily = "Kanit"
const fontFamilyAndroid = "kanit_light_android"

const borderRadiusContent = 15

module.exports = {
    size,
    color,
    shadow,
    fontFamily,
    fontFamilyAndroid,
    cardPrimary,
    borderRadiusContent,
    cardWhite
}